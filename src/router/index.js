import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
import store from '@/store';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes,
});

router.beforeEach((to, from, next) => {
  if (!store.getters['auth/isAuthenticated'] && to.matched.some((route) => route.meta.protected)) {
    next({
      name: 'login',
      query: { redirect: to.name },
    });
  } else {
    next();
  }
});


export default router;