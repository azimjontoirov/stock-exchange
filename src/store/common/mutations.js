export function setTitle(state, title) {
  state.title = title;
}

export function setLoading(state, isLoading) {
  state.loading = isLoading;
}

export function setError(state, error) {
  state.error = error;
}

export function clearError(state) {
  state.error = null;
}

export function setCalculate(state, price) {
  state.calculateResult -= price;
}

export function setIssuers(state, issuers) {
  state.issuers = issuers
}