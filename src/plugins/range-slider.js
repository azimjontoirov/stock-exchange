/**
 *
 * Author:  Azimjon Toirov
 * Created: 12.05.2021 21:47
 */

import Vue from 'vue'
import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/material.css'

Vue.component('VueSlider', VueSlider)