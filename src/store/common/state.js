export default function () {
  return {
    loading: false,
    error: null,
    title: '',
    calculateResult: 1000000,
    issuers: [],
  };
}