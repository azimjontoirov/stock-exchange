export function signIn(state, payload) {
  state.token = payload.api_token;
  state.user = payload.user;
}

export function signUp(state, payload) {
  state.token = payload.isAuthenticated;
  state.user = payload.user;
}

export function signOut(state) {
  state.token = false;
}