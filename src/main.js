import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import i18n from './i18n/i18n';

import $ from 'jquery';
import 'popper.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Toasted from 'vue-toasted';

// styles
import './assets/css/app.css';
import './assets/scss/app.scss';

// plugins
import './plugins/range-slider';
import './plugins/vue-tabs';
import './plugins/star-rating';
import './plugins/vue-loader';

import './utils/custom-axios';
import toast from './utils/toast';

import './assets/js/pre-loader';

window.$ = $;
AOS.init();

Vue.config.productionTip = false;

Vue.use(Toasted);
Vue.use(toast);
new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
