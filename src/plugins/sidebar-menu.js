/**
 *
 * Author:  Azimjon Toirov
 * Created: 22.05.2021 19:25
 */

import Vue from 'vue';
import VueSidebarMenu from 'vue-sidebar-menu';
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css';

Vue.use(VueSidebarMenu);