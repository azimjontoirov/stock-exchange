/**
 *
 * Author:  Azimjon Toirov
 * Created: 28.07.2021 21:17
 */
import Vue from 'vue';
import axios from 'axios';
import Cookie from 'js-cookie';
import config from '../config'

axios.defaults.baseURL = config.baseURL;

const requestHandler = (request) => {
  const token = Cookie.get('token');
  request.headers.common['Authorization'] = token ? `Bearer ${token}` : '';

  return request;
};

axios.interceptors.request.use(request => requestHandler(request));

Vue.prototype.$axios = axios;