/**
 *
 * Author:  Azimjon Toirov
 * Created: 23.05.2021 2:00
 */
import Vue from 'vue';
import VueTabs from 'vue-nav-tabs';
import 'vue-nav-tabs/themes/vue-tabs.css';

Vue.use(VueTabs);