/**
 *
 * Author:  Azimjon Toirov
 * Created: 26.05.2021 12:02
 */
import Vue from 'vue';
import VueStarRating from 'vue-star-rating'

Vue.component('star-rating', VueStarRating)