export function login({ commit }, payload) {
  commit('signIn', payload);
}

export function register({ commit }, payload) {
  commit('signUp', payload);
}

export function logout({ commit }) {
  commit('signOut');
}