/**
 *
 * Author:  Azimjon Toirov
 * Created: 31.07.2021 19:15
 */

export default {
  install(Vue) {
    Vue.prototype.$toastError = function (text) {
      return this.$toasted.error(text, {
        duration: 5000,
        action: {
          text: 'Закрить',
          onClick: (e, toastObject) => {
            toastObject.goAway(0);
          },
        },
      });
    };
    Vue.prototype.$toastSuccess = function (text) {
      return this.$toasted.success(text, {
        duration: 5000,
        action: {
          text: 'Закрить',
          onClick: (e, toastObject) => {
            toastObject.goAway(0);
          },
        },
      });
    };
  },
};


