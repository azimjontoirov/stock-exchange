import axios from 'axios';

export function setLoading({ commit }, payload) {
  commit('setLoading', payload);
}

export function setError({ commit }, payload) {
  commit('setError', payload);
}

export function clearError({ commit }) {
  commit('clearError');
}

export function title({ commit }, title) {
  commit('setTitle', title);
}

export function calculate({ commit }, { sum }) {
  commit('setCalculate', sum);
}

export async function getIssuers({ commit }) {
  try {
    const { data: { data: issuers } } = await axios.get(`issuer`);
    commit('setIssuers', issuers.issuers.data);

  } catch (e) {
    // Vue.$toastError(e.message);
  }

}