export function pageTitle(state) {
  return state.title;
}

export function isLoading(state) {
  return state.loading;
}

export function error(state) {
  return state.error;
}

export function getCalculate(state) {
  return state.calculateResult
}

export function issuers(state) {
  return state.issuers;
}