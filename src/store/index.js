import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import common from './common';
import createPersistedState from 'vuex-persistedstate';
import Cookie from 'js-cookie';

Vue.use(Vuex);

const authState = new createPersistedState({
  key: 'auth',
  paths: ['auth'],
  storage: {
    setItem: (key, value) => Cookie.set(key, value, { expires: 1 }),
    getItem: (key) => Cookie.get(key),
    removeItem: (key) => Cookie.remove(key),
  },
});

const commonState = new createPersistedState({
  key: 'common',
  paths: ['common'],
});

const store = new Vuex.Store({
  modules: {
    auth,
    common,
  },
  strict: process.env.DEBUGGING,
  plugins: [authState, commonState],
});

export default store;
