import AuthShell from '../shells/AuthShell';
import MainShell from '../shells/MainShell';
import EmptyShell from '../shells/EmptyShell';

export const routes = [
  {
    path: '/',
    name: 'home',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/HomePage'),
  },
  {
    path: '/briefcase',
    name: 'briefcase',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/BriefcasePage'),
  },
  {
    path: '/securities',
    name: 'securities',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/SecuritiesPage'),
  },
  {
    path: '/brokers',
    name: 'brokers',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/BrokersPage'),
  },
  {
    path: '/my-brokers',
    name: 'my-brokers',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/MyBrokersPage'),
  },
  {
    path: '/brokers/:brokerId',
    name: 'broker',
    meta: {
      shell: MainShell,
      protected: true,
    },
    props: true,
    component: () => import('../pages/BrokerPage'),
  },
  {
    path: '/issuers',
    name: 'issuers',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/IssuersPage'),
  },
  {
    path: '/my-issuers',
    name: 'my-issuers',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/MyIssuersPage'),
  },
  {
    path: '/issuer/:issuerId',
    name: 'issuer',
    meta: {
      shell: MainShell,
      protected: true,
    },
    props: true,
    component: () => import('../pages/IssuerPage'),
  },
  {
    path: '/knowledge-base',
    name: 'knowledge-base',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/KnowladgeBasePage'),
  },
  {
    path: '/calculator',
    name: 'calculator',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/CalculatorPage'),
  },
  {
    path: '/screener',
    name: 'screener',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/ScreenerPage'),
  },
  {
    path: '/settings',
    name: 'settings',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/SettingsPage'),
  },
  {
    path: '/faq',
    name: 'faq',
    meta: {
      shell: MainShell,
      protected: true,
    },
    component: () => import('../pages/FAQPage'),
  },
  {
    path: '/company/:companyId',
    name: 'company',
    meta: {
      shell: MainShell,
      protected: true,
    },
    props: true,
    component: () => import('../pages/CompanyPage'),
  },
  {
    path: '*',
    name: 'error',
    meta: {
      shell: EmptyShell,
    },
    component: () => import('../pages/error/ErrorPage'),
  },
  {
    path: '/sign-in',
    name: 'login',
    meta: {
      shell: AuthShell,
    },
    component: () => import('../pages/auth/LoginPage'),
  },
  {
    path: '/sign-up',
    name: 'register',
    meta: {
      shell: AuthShell,
    },
    component: () => import('../pages/auth/RegisterPage'),
  },
];