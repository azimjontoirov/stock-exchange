/**
 *
 * Author:  Azimjon Toirov
 * Created: 02.08.2021 21:50
 */

import Vue from 'vue';
import VueLoaders from 'vue-loaders'
import 'vue-loaders/dist/vue-loaders.css'

Vue.use(VueLoaders);
