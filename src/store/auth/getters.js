export function isAuthenticated(state) {
  return !!state.token;
}

export function user(state) {
  return state.user;
}